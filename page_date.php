<?php
$date = [
	"time"=> date("H:i:s"),
	"date"=> date("j.m.Y"),
	"dayOfWeek"	=> date("w")
];
$date = json_encode($date);
echo $date;
